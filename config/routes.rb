Rails.application.routes.draw do
  mount Hyperstack::Engine => '/hyperstack'  # this route should be first in the routes file so it always matches

  # legacy routes on top
  devise_for :users

  get '/(*others)', to: 'hyperstack#app'

  root to: 'hyperstack#app'
end

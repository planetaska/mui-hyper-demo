//app/javascript/packs/client_and_server.js
// these packages will be loaded both during prerendering and on the client
React = require('react');                      // react-js library
History = require('history');                  // react-router history library
ReactRouter = require('react-router');         // react-router js library
ReactRouterDOM = require('react-router-dom');  // react-router DOM interface
ReactRailsUJS = require('react_ujs');          // interface to react-rails
// to add additional NPM packages run `yarn add package-name@version`
// then add the require here.

// style and theme
MuiStyles = require('@material-ui/core/styles');
global.MuiStyles = MuiStyles;

MuiThemeProvider = require('@material-ui/styles/ThemeProvider');
global.MuiThemeProvider = MuiThemeProvider;

// css baseline
CssBaseline = require('@material-ui/core/CssBaseline');
global.CssBaseline = CssBaseline;

// container and grid
Container = require('@material-ui/core/Container');
global.Container = Container;

Grid = require('@material-ui/core/Grid');
global.Grid = Grid;

Box = require('@material-ui/core/Box');
global.Box = Box;

Paper = require('@material-ui/core/Paper');
global.Paper = Paper;

// components
// icons
Icon = require('@material-ui/core/Icon');
global.Icon = Icon;

Avatar = require('@material-ui/core/Avatar');
global.Avatar = Avatar;

// link
MuiLink = require('@material-ui/core/Link');
global.MuiLink = MuiLink;

// buttons
Button = require('@material-ui/core/Button');
global.Button = Button;

IconButton = require('@material-ui/core/IconButton');
global.IconButton = IconButton;

// appbar
AppBar = require('@material-ui/core/AppBar');
global.AppBar = AppBar;

ToolBar = require('@material-ui/core/ToolBar');
global.ToolBar = ToolBar;

// typography
Typography = require('@material-ui/core/Typography');
global.Typography = Typography;

// menu
Menu = require('@material-ui/core/Menu');
global.Menu = Menu;

MenuItem = require('@material-ui/core/MenuItem');
global.MenuItem = MenuItem;

// drawer
Drawer = require('@material-ui/core/Drawer');
global.Drawer = Drawer;

// list
List = require('@material-ui/core/List');
global.List = List;

ListItem = require('@material-ui/core/ListItem');
global.ListItem = ListItem;

ListItemIcon = require('@material-ui/core/ListItemIcon');
global.ListItemIcon = ListItemIcon;

ListItemText = require('@material-ui/core/ListItemText');
global.ListItemText = ListItemText;

ListSubheader = require('@material-ui/core/ListSubheader');
global.ListSubheader = ListSubheader;

Collapse = require('@material-ui/core/Collapse');
global.Collapse = Collapse;

ListItemSecondaryAction = require('@material-ui/core/ListItemSecondaryAction');
global.ListItemSecondaryAction = ListItemSecondaryAction;

// divider
Divider = require('@material-ui/core/Divider');
global.Divider = Divider;

// form inputs
FormControlLabel = require('@material-ui/core/FormControlLabel');
global.FormControlLabel = FormControlLabel;

TextField = require('@material-ui/core/TextField');
global.TextField = TextField;

Checkbox = require('@material-ui/core/Checkbox');
global.Checkbox = Checkbox;

// date picker
DateFnsUtils = require('@date-io/date-fns');
global.DateFnsUtils = DateFnsUtils;

MuiPickersUtilsProvider = require('@material-ui/pickers/MuiPickersUtilsProvider');
global.MuiPickersUtilsProvider = MuiPickersUtilsProvider;

DatePicker = require('@material-ui/pickers/DatePicker');
global.DatePicker = DatePicker;

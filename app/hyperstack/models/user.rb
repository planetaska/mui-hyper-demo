class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable unless RUBY_ENGINE == 'opal'

  has_many :items, dependent: :destroy

  def self.current
    Hyperstack::Application.acting_user_id ? find(Hyperstack::Application.acting_user_id) : User.new
  end
end

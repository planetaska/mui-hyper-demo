class CreateUserOperation < Hyperstack::ControllerOp
  param :name
  param :email
  param :password

  add_error :email, :already_exist, 'Email already exist' do
    @user = User.find_by_email(params.email)
    abort! if !@user.nil?
  end

  step do
    User.create(
        name: params.name,
        email: params.email,
        password: params.password
    )
  end
end

class SigninOperation < Hyperstack::ControllerOp
  param :email
  param :password

  add_error :email, :doesnt_exist, 'Incorrect Email or password' do
    @user = User.find_by_email(params.email)
    abort! if @user.nil?
  end
  #step { @user = User.find_by_email(params.email) }
  #step { fail if @user.nil? }
  #failed { abort! }

  add_error :password, :incorrect, 'Incorrect Email or password' do
    abort! unless @user.valid_password? params.password
  end
  #step { fail unless @user.valid_password? params.password }
  #failed { abort! }
  #step { Hyperstack::ClientDrivers.opts[:acting_user_id] = @user.id }
  step { sign_in @user }
end

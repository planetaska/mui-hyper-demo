class SignoutOperation < Hyperstack::ControllerOp
  step { sign_out :user }
end

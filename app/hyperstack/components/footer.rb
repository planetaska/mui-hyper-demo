class Footer < HyperComponent
  STYLES = {
      footer: {
        marginTop: '60px',
        padding: '30px'
      }
  }.freeze

  render do
    FOOTER(style:STYLES[:footer]) do
      Container do
        Typography(variant:'h6', align:'center', gutterBottom: true) do
          'Hyperstack'
        end
        Typography(variant:'subtitle1', align:'center', color:'textSecondary', component:'p') do
          'The next generation of web'
        end
      end
    end
  end
end

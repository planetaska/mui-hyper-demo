class Header < HyperComponent
  #include Hyperstack::Router
  include Hyperstack::Router::Helpers

  STYLES = {
    root: {
      flexGrow: 1,
      marginBottom: '20px'
    },
    menuButton: {
      marginRight: '20px'
    },
    title: {
      flexGrow: 1
    },
    nested: {
        paddingLeft: '40px'
    },
    drawer_width: {
        width: '260px'
    }
  }.freeze

  def toggleDrawer
    mutate @open = !@open
  end

  def toggleItems
    mutate @open_items = !@open_items
  end

  before_mount do
    @open = false
    @open_items = false
  end

  render(HEADER) do
    DIV(style: STYLES[:root]) do
      AppBar(position: 'static') do
        Container(max_width: 'sm') do
          ToolBar do
            IconButton(edge:'start', style:STYLES[:menuButton], 'aria-label':'Menu') do
              Icon {'menu'}
            end
              .on(:click) { toggleDrawer }
            Typography(variant:'h6', style: STYLES[:title]) do
              'Hyper MUI Demo'
            end
            IconButton(style:STYLES[:menuButton], 'aria-label':'Home', component:`ReactRouterDOM.Link`, to: '/') do
              Icon {'home'}
            end
            if User.current.new?
              Button(component:`ReactRouterDOM.Link`, to: '/sign_in') do
                'Login'
              end
            else
              Button do
                'Log out'
              end
              .on(:click) do
                SignoutOperation.then do
                  `window.location = '/sign_in'`
                end
              end
            end
          end
        end
      end
    end
    Drawer(open: @open) do
      DIV(style:STYLES[:drawer_width]) do
        IconButton do
          Icon {'chevron_left'}
        end
      end
          .on(:click) { toggleDrawer }
      Divider()
      List do
        ListItem(button: true, component:`ReactRouterDOM.Link`, to: '/items') do
          ListItemIcon do
            Icon {'extension'}
          end
          ListItemText(primary: 'Items')
          ListItemSecondaryAction do
            IconButton(edge:'end', 'aria-label':'Expand') do
              Icon {'expand_more'}
            end
          end
              .on(:click) { toggleItems }
        end

        Collapse(in: @open_items, timeout:'auto', unmountOnExit: true) do
          List(component:'div', disablePadding: true) do
            ListItem(button: true, key:'My Items', style:STYLES[:nested]) do
              ListItemIcon do
                Icon {'extension'}
              end
              ListItemText(primary: 'My Items')
            end
          end
        end
      end
      Divider()
      List do
        ListItem(button: true, component:`ReactRouterDOM.Link`, to: '/users') do
          ListItemIcon do
            Icon {'people'}
          end
          ListItemText(primary: 'Users')
        end
      end
    end
  end
end

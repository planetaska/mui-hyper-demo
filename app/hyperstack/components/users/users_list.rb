class UsersList < HyperComponent
  include Hyperstack::Router::Helpers
  render(SECTION) do
    UserDialog(user: User.new)
    TABLE do
      THEAD do
        TR do
          TH { 'Name' }
          TH { 'Email' }
          TH { 'Action' }
        end
      end
      TBODY do
        user_rows
      end
    end
    UL do
      User.each do |user|
        LI { user.email }
      end
    end
  end

  def user_rows
    User.each do |user|
      TR do
        TD { user.name }
        TD { user.email }
        TD do
          UserDialog(user: user, key: user.id)
          BUTTON(type: :button) { 'Delete' }
            .on(:click) { user.destroy }
        end
      end
    end
  end
end

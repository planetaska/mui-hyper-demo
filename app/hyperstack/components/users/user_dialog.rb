class UserDialog < HyperComponent
  #include Hyperstack::Router::Helpers

  param :user

  before_mount do
    reset_mutations
  end

  render do
    if @open
      render_dialog
    else
      edit_or_new_button.on(:click) { mutate @open = true }
    end
  end

  def render_dialog
    DIV do
      H3 do
        'User'
      end
      DIV do
        content
        error_messages if @errors.any?
      end
      DIV do
        actions
      end
    end
  end

  def edit_or_new_button
    if user.new?
      INPUT(type: :button, value: 'New')
    else
      INPUT(type: :button, value: 'Edit')
    end
  end

  def content
    FORM do
      INPUT(type: :text, value: @user_name).on(:change) do |e|
        mutate @user_name = e.target.value
        validate_content
      end
      INPUT(type: :email, value: @email).on(:change) do |e|
        mutate @email = e.target.value
        validate_content
      end

      if user.new?
        INPUT(type: :password, value: @password).on(:change) do |e|
          mutate @password = e.target.value
          validate_content
        end
        INPUT(type: :password, value: @password_confirmation).on(:change) do |e|
          mutate @password_confirmation = e.target.value
          validate_content
        end
      end
    end
  end

  def actions
    INPUT(type: :button, value: 'Cancel').on(:click) { cancel }

    INPUT(type: :submit, disabled: !@valid).on(:click) do
      if user.new?
        create_user
      else
        save
      end
    end
  end

  def create_user
    CreateUserOperation.run(name: @user_name, email: @email, password: @password)
      .then do
        #mutate @open = false
        reset_mutations
      end
      .fail do |e|
        mutate @errors = e.errors.message
      end
  end

  def save
    user.name = @user_name
    user.email = @email
    user.save(validate: true).then do |result|
      if result[:success]
        mutate @open = false
      else
        mutate @errors = user.errors.full_messages
      end
    end
  end

  def cancel
    user.revert
    mutate @open = false
  end

  def error_messages
    @errors.each do |message|
      P { message }
    end
  end

  def validate_content
    mutate @valid = false
    return if @user_name.empty?
    return if @email.empty?
    return unless @email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)

    if user.new?
      return unless @password.length >= 6
      return unless @password_confirmation.length >= 6
      return unless @password == @password_confirmation
    end

    #true
    mutate @valid = true
  end

  def reset_mutations
    @open = false
    @user_name = user.name
    @email = user.email
    @password = ''
    @password_confirmation = ''
    @valid = false
    @errors = []
  end
end

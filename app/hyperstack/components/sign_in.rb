class SignIn < HyperComponent
  include Hyperstack::Router::Helpers

  STYLES = {
    paper: {
      marginTop: '80px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    },
    avatar: {
      margin: '10px',
      backgroundColor: '#90CAF9'
    },
    form: {
      width: '100%',
      marginTop: '10px'
    },
    submit: {
      marginTop:'30px',
      marginBottom:'20px'
    },
  }.freeze

  before_mount do
    @email = ''
    @password = ''
    @valid = false
    @errors = []
  end

  render(DIV) do
    Container(component:'main', maxWidth:'xs') do
      DIV(style:STYLES[:paper]) do
        Avatar(style:STYLES[:avatar]) do
          Icon {'lock_outlined'}
        end
        Typography(component:'h1', variant:'h5')do
          'Sign in'
        end
        FORM(style:STYLES[:form]) do
          TextField( value: @email,
            variant:"outlined", margin:"normal", required: true, fullWidth: true,
            id:"email", label:"Email Address", name:"email", autoComplete:"email",
          ).on(:change) do |e|
            mutate @email = e.target.value
            @valid = validate_content
          end

          TextField( value: @password, type: 'password',
            variant:"outlined", margin:"normal", required: true, fullWidth: true,
            id:"password", label:"Password", name:"password", autoComplete:"current-password"
          ).on(:change) do |e|
            mutate @password = e.target.value
            @valid = validate_content
          end

          MuiPickersUtilsProvider(utils: DateFnsUtils) do
              DatePicker(value: @date_from)
          end

          #Checkbox(value:'remember')
          #SPAN {'Remember'}
          FormControlLabel(control:Checkbox(value:'remember').as_node.to_n, label: 'Remember')

          error_messages if @errors.any?

          Button(
              type:'submit', fullWidth: true, variant:'contained',
              color:'secondary', style:STYLES[:submit], disabled: !@valid
          ) do
            'Sign In'
          end

          Grid(container:true) do
            Grid(item:true, xs:true) do
              MuiLink(href:'#', variant:'body2', color:'secondary') do
                'Forgot password?'
              end
            end
          end
        end
        .on(:submit) do |e|
          e.prevent_default
          SigninOperation.run(email: @email, password: @password)
            .then do
            `window.location = '/'`
            end
            .fail do |e|
            mutate @errors = e.errors.message
            end
        end
      end
    end
  end

  def validate_content
    return false if @password.to_s.length < 6
    return false if @email.to_s.empty?
    return false unless @email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)

    true
  end

  def error_messages
    @errors.each do |message|
      Typography(component:'p', variant:'body2', color:'error') do
        message
      end
    end
  end
end

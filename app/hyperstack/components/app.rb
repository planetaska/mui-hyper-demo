class App < HyperComponent
  include Hyperstack::Router
  include Hyperstack::Router::Helpers

  before_mount do
    THEME = %x{
      MuiStyles.createMuiTheme({
        palette: {
          primary: { main: '#E1F5FE', text: {color: '#000000'} },
          secondary: { main: '#90CAF9', text: {color: '#E1F5FE'} }
        }
      })
    }
  end

  after_mount do
    if User.current.new?
      history.replace('/sign_in')
    end
  end

  render(SECTION) do
    MuiThemeProvider(theme: THEME) do
      CssBaseline()
      Header()

      Container do
        Route('/', exact: true, mounts: Index)
        Route('/sign_in', mounts: SignIn)
        Route('/users', mounts: UsersList)
        #Route('/items', mounts: ItemsIndex)
      end

      Footer()
    end
  end
end

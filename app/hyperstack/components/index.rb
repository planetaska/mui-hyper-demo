class Index < HyperComponent
  include Hyperstack::Router::Helpers
  render(SECTION) do
    DIV do
      SPAN {"Welcome, #{User.current.name}"}
    end
  end
end
